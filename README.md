# progetto_TW



## Scenario applicativo

• Python versione 3.10
• Pipenv (Nel Pipfile del progetto sono indicate le dipendenze che verrano installate con il
        comando pipenv install)
• Django Framework
• django-crispy-forms
• pillow (per l'upload delle immagini)
• django-next-prev
• django braces
• channels
• daphne

Da CDN

• bootstrap

## Passaggi

Dopo aver creato una cartella ed esservi entrati, effettuare

>> git clone https://gitlab.com/tecnologie_web/progetto_tw.git


Installare l'ambiente virtuale

>> pipenv install

Attivare l'ambiente virtuale

>> pipenv shell

Avviare il server

>> python manage.py runserver

Nota: All'avvio viene inizializzato il DB con la creazione di alcuni utenti, servizi e appuntamenti (e la
creazione di Gruppi con relativi permessi a cui vengono aggiunti gli utenti creati).

Per questo assicurarsi che la funzione initdb() in ivonne/urls.py non sia commentata