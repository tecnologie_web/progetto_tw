"""
ASGI config for ivonne project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from .routing import ws_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ivonne.settings')

#application = get_asgi_application()

# ProtocolTypeRouter permette di definire regole di routing a partire dal root project
# e come protocolli diversi debbano essere serviti
application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AuthMiddlewareStack(URLRouter(ws_urlpatterns))
    }     #URLRouter traduce gli url per gli endpoint dei nostri websocket consumers
)         # in regole di routing (descritte nel file routing.py)
