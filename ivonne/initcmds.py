import os
import random
from .settings import MEDIA_ROOT
from gestione.models import Servizio, Appuntamento, Coda
from datetime import datetime
from datetime import timedelta, time
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType


def erase_db() :
    print("Cancello il DB")
    Appuntamento.objects.all().delete()
    Coda.objects.all().delete()
    Servizio.objects.all().delete()
    Group.objects.all().delete()
    u = User.objects.all().exclude(username='admin')
    u.delete()
    p1 = Permission.objects.get(codename='can_add_appuntamento')
    p1.delete()
    p2 = Permission.objects.get(codename='can_delete_appuntamento')
    p2.delete()
    p3 = Permission.objects.get(codename='can_view_appuntamento')
    p3.delete()
    p4 = Permission.objects.get(codename='can_view_servizio')
    p4.delete()
    p5 = Permission.objects.get(codename='can_add_servizio')
    p5.delete()
    p6 = Permission.objects.get(codename='can_delete_servizio')
    p6.delete()
    p7 = Permission.objects.get(codename='can_change_servizio')
    p7.delete()


def init_db() :
    #path = MEDIA_ROOT / "init_images"
    #jpgs = [os.path.join(path, file)
            #for file in os.listdir(path)
            #if file.endswith('.jpg')]
    #print(jpgs)
    if len(Servizio.objects.all()) != 0 :
        return
    datetime_list = []
    time_list = []
    servizidict = {
                   "nomi" : ["Piega Zen", "Color WOW!"],
                   "minuti" : [120, 90], "testi" : ["Lorem ipsum dolor sit amet", "Lorem ipsum dolor sit amet"]
                   }
    userprofiles = [['amber', 'upthathill@hotmail.it', 'amber'], \
                    ['rocha', 'tourincanto@gmail.com', 'rocha'], \
                    ['lamb', 'bernini.am@gmail.com', 'lamb']]
    today = datetime.today()
    tomorrow = today + timedelta(days=1)
    # after_tomorrow = tomorrow + datetime.timedelta(days=1)
    date_list = [today, tomorrow]
    string_time_list = ['08:00', '10:00', '12:00', '14:30', '16:30']
    for stl in string_time_list :
        tl = time.fromisoformat(stl)
        time_list.append(tl)

    # creazione di gruppi
    customer_group, created = Group.objects.get_or_create(name="Clienti")
    creative_group, created = Group.objects.get_or_create(name="Creativi")
    ct_a = ContentType.objects.get_for_model(Appuntamento)
    ct_s = ContentType.objects.get_for_model(Servizio)

    perm_ad_a = Permission.objects.create(codename='can_add_appuntamento',
                                           name='Can add appuntamento',
                                           content_type=ct_a)
    per_del_a = Permission.objects.create(codename='can_delete_appuntamento',
                                           name='Can delete appuntamento',
                                           content_type=ct_a)
    per_view_a = Permission.objects.create(codename='can_view_appuntamento',
                                           name='Can view appuntamento',
                                           content_type=ct_a)
    per_view_s = Permission.objects.create(codename='can_view_servizio',
                                           name='Can view servizio',
                                           content_type=ct_s)
    perms = [perm_ad_a, per_del_a, per_view_a, per_view_s]

    for perm in perms:
        customer_group.permissions.add(perm)

    per_ad_s = Permission.objects.create(codename='can_add_servizio',
                                           name='Can add servizio',
                                           content_type=ct_s)

    per_del_s = Permission.objects.create(codename='can_delete_servizio',
                                          name='Can delete servizio',
                                          content_type=ct_s)

    per_ch_s = Permission.objects.create(codename='can_change_servizio',
                                           name='Can change servizio',
                                           content_type=ct_s)

    more_perms = [per_ad_s, per_del_s, per_ch_s]

    for mp in more_perms:
        perms.append(mp)

    for perm in perms:
        creative_group.permissions.add(perm)


    # creazione clienti
    for u in userprofiles :
        user = User.objects.create_user(username=u[0],
                                        email=u[1],
                                        password=u[2])
        g = Group.objects.get(name="Clienti")  # cerco il gruppo che mi interessa
        g.user_set.add(user)
        user.save()
    # creazione utente creativo (titolare dello Studio)
    creative_user = User.objects.create_user(username='ivonne',
                                             email='yvonne.perottoni@gmail.com',
                                             password='ivonne')
    cg = Group.objects.get(name="Creativi")
    cg.user_set.add(creative_user)
    creative_user.save()
    users = User.objects.all().exclude(username=['admin', 'ivonne'])
    for i in range(len(servizidict["nomi"])) :
        s = Servizio()
        for k in servizidict :
            if k == "nomi" :
                s.nome = servizidict[k][i]
            if k == "minuti" :
                s.minuti = servizidict[k][i]
            if k == "testi" :
                s.testo = servizidict[k][i]
        s.save()
    services = Servizio.objects.all()
    for d in date_list :
        for t in time_list :
            date_time = datetime.combine(d, t, tzinfo=timezone.get_current_timezone())
            datetime_list.append(date_time)
    for dt in datetime_list :
        inu = random.randint(0, len(users) - 1)
        ins = random.randint(0, len(services) - 1)
        a = Appuntamento()
        a.data_ora_inizio = dt
        a.data_ora_fine = a.data_ora_inizio + timedelta(minutes=services[ins].minuti)
        a.cliente = users[inu]
        a.servizio = services[ins]
        a.save()

    print("DUMP DB")
    print(Servizio.objects.all())
    print(Appuntamento.objects.all())


def db_from_now_on() :
    for i in Appuntamento.objects.all() :
        if i.data_ora_fine < timezone.localtime(timezone.now()) :
            i.delete()
