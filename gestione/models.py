from django.core.validators import FileExtensionValidator
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Servizio(models.Model) :
    immagine = models.ImageField(upload_to='uploaded_images', null=True, blank=True)
    video = models.FileField(upload_to='uploaded_videos', null=True, blank=True,
                             validators=[
                                 FileExtensionValidator(allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv'])])
    nome = models.CharField(max_length=50)
    minuti = models.IntegerField(default=30)
    testo = models.TextField(max_length=255)

    class Meta :
        verbose_name_plural = "Servizi"

    def __string__(self):
        return str(self.nome)


class Appuntamento(models.Model) :
    data_ora_inizio = models.DateTimeField(default=None, null=True, blank=True)
    data_ora_fine = models.DateTimeField(default=None, null=True, blank=True)
    servizio = models.ForeignKey(Servizio, on_delete=models.CASCADE, related_name="appuntamento")
    cliente = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, default=None,
                                related_name="appuntamenti_fissati")

    class Meta :
        verbose_name_plural = "Appuntamenti"
        # criterio di ordinamento per il modulo next-prev nelle views
        ordering = ['data_ora_inizio']

    #def __string__(self):
    #    return self.name


class Coda(models.Model) :
    data_ora_inizio = models.DateTimeField(default=None, null=True, blank=True)
    data_ora_fine = models.DateTimeField(default=None, null=True, blank=True)
    servizio = models.ForeignKey(Servizio, on_delete=models.CASCADE, related_name="servizio_richiesto")
    cliente = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, default=None,
                                related_name="lista_attesa")


