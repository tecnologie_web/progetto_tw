from django.shortcuts import render
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime
from datetime import timedelta, time
from next_prev import *
from django.core.mail import *
from braces.views import GroupRequiredMixin


# Create your views here.
def gestione_home(request) :
    return render(request, template_name="gestione/home.html")


class ServizioListView(ListView) :
    titolo = "I Servizi di Ivonne!"
    model = Servizio
    template_name = "gestione/lista_servizi.html"


def search(request) :
    if request.method == "POST" :
        form = SearchForm(request.POST)
        if form.is_valid() :
            sstring = form.cleaned_data.get("search_string")
            where = form.cleaned_data.get("search_where")
            return redirect("gestione:ricerca_risultati", sstring, where)
    else :
        form = SearchForm()
    return render(request, template_name="gestione/ricerca.html", context={"form" : form})


class ServizioRicercaView(ServizioListView) :
    titolo = "La ricerca ha prodotto i seguenti risultati"

    def get_queryset(self) :
        sstring = self.request.resolver_match.kwargs["sstring"]
        where = self.request.resolver_match.kwargs["where"]

        if "Nome" in where :
            qq = self.model.objects.filter(nome__icontains=sstring)
        else :
            qq = self.model.objects.filter(minuti__icontains=sstring)
        return qq

# prende i dati dal BookForm e li ricostruisce opportunamente
# nel form ho messo due campi distinti per data e ora e voglio ricavare
# un unico datetime (con la funzione combine()) da inserire nel model Appuntamento
# con la funzione combine l'oggetto restituito è timezone unaware, occorre dunque specificare tzinfo
def from_form(form) :
    giorno = form.cleaned_data.get("giorno")
    ora = form.cleaned_data.get("ora")
    mins = form.cleaned_data.get("mins")
    if giorno == None:
        when = None
    else:
        o = ora.split(":")
        m = mins.split(":")
        string = o[0] + ":" + m[1]
        orario = time.fromisoformat(string)
        when = datetime.combine(giorno, orario, tzinfo=timezone.get_current_timezone())
    return when

@login_required
def search_reservation(request) :
    if request.user.groups.filter(name='Creativi').exists():
        if request.method == "POST" :
            form = AppuntamentoSearchForm(request.POST)
            if form.is_valid():
                where = form.cleaned_data.get("search_where")
                if where == "Cliente":
                    sstring = form.cleaned_data.get("search_string")
                    if sstring == '':
                        return redirect("gestione:ricerca_appuntamento")
                    #print(sstring)
                else:
                    sstring = from_form(form)
                if sstring == None:
                    return redirect("gestione:ricerca_appuntamento")
                else:
                    return redirect("gestione:appuntamento_risultati", sstring, where)
        else :
            form = AppuntamentoSearchForm()
    else:
        return redirect("gestione:situazione")
    return render(request, template_name="gestione/ricerca_appuntamento.html", context={"form" : form})


@login_required
def save_the_date(request, pk) :
    if request.user.groups.filter(name='Creativi').exists() :
        if request.method == "POST" :
            form = CreativeBookForm(request.POST)
            if form.is_valid() :
                when = from_form(form)
                who = form.cleaned_data.get("cliente")
                return redirect("gestione:prenota_risultati", pk, when, who)
        else :
            form = CreativeBookForm()
    else :
        if request.method == "POST" :
            form = BookForm(request.POST)
            if form.is_valid() :
                when = from_form(form)
                who = None
                return redirect("gestione:prenota_risultati", pk, when, who)
        else :
            form = BookForm()

    return render(request, template_name="gestione/prenota.html", context={"form" : form})


limite_min = timezone.localtime(timezone.now())


# funzione che, dato un datetime, calcola il giorno successivo alle ore 8
# evitando che sia di domenica o lunedì
def next_day_8(date_time) :
    data = date_time.date()
    # se la data è sabato, il giorno successivo è dopo 3 giorni
    if datetime.weekday(data) == 5 :
        next_day = data + timedelta(days=3)
    # se è domenica, il giorno successivo è dopo 2 giorni
    elif datetime.weekday(data) == 6 :
        next_day = data + timedelta(days=2)
    # altrimenti aggiungi un solo giorno
    else :
        next_day = data + timedelta(days=1)
    # costruisci il giorno successivo alle 8 (timezone aware)
    hour = '08:00'
    start_hour = time.fromisoformat(hour)
    tz_next_day = datetime.combine(next_day, start_hour, tzinfo=timezone.get_current_timezone())
    return tz_next_day


# funzione che, per ogni appuntamento, valuta se il tempo a disposizione tra un appuntamento
# e il successivo è sufficiente per il servizio indicato nell'argomento e
# restituisce due date che soddisfino tale requisito
def nuove_date(servizio) :
    max_hour = ('16:30:00')
    aware_max_hour = timezone.make_aware(time.fromisoformat(max_hour))
    new_dates = []
    if Appuntamento.objects.all().count() == 0:
        new_date = timezone.now()
        new_dates.append(new_date)
        new_date2 = new_date + timedelta(minutes=servizio.minuti)
        new_dates.append(new_date2)
    else:
        ordered_db = Appuntamento.objects.all().order_by('data_ora_inizio')
        for i in ordered_db :
            #considera gli appuntamenti futuri
            if i.data_ora_inizio >= limite_min :
                i_time = timezone.make_aware(i.data_ora_fine.time())
                next_item = next_in_order(i)
                # A) se non ci sono appuntamenti successivi da confrontare
                if next_item == None :
                    # e se la data di fine dell'appuntamento precedente eccede l'orario massimo
                    if i_time > aware_max_hour :
                        # la prima data disponibile è l'inizio del giorno successivo
                        new_date = next_day_8(i.data_ora_fine)
                        new_dates.append(new_date)
                        if len(new_dates) == 2 :
                            break
                        else :
                            # proponi la seconda data nel pomeriggio
                            new_date2 = new_date + timedelta(hours=7)
                            new_dates.append(new_date2)
                    else :
                        new_date = i.data_ora_fine
                        new_dates.append(new_date)
                        if len(new_dates) == 2 :
                            break
                        else :
                            new_date2 = next_day_8(new_date)
                            new_dates.append(new_date2)
                # B) se c'è un appuntamento successivo da confrontare
                else :
                    if i_time > aware_max_hour :
                        fine = next_day_8(i.data_ora_fine)
                    else :
                        fine = i.data_ora_fine
                    difference = next_item.data_ora_inizio - fine
                    # converto la differenza in integer per confrontarla con la durata del servizio
                    difference = difference / timedelta(minutes=1)
                    if int(difference) >= servizio.minuti :
                        new_date = fine
                        new_dates.append(new_date)
                    if len(new_dates) == 2 :
                        break
        else:
            i.delete()
    return new_dates


@login_required
def reservation(request, pk, when, who) :
    date_alternative = [None, None]
    s = get_object_or_404(Servizio, pk=pk)
    a = Appuntamento()
    whenOK = datetime.fromisoformat(when)
    # impedire prenotazioni nel passato
    # con limite_min variabile esterna alla funzione (perché riutilizzata altrove)
    # e impedire la prenotazione nei giorni di chiusura (lunedì=0, domenica=6)
    when_weekday = datetime.weekday(whenOK)
    if when_weekday != 0 and when_weekday != 6 :
        if whenOK > limite_min :
            a.data_ora_inizio = whenOK
            a.data_ora_fine = whenOK + timedelta(minutes=s.minuti)
            a.servizio = s
            if who == 'None' :
                a.cliente = request.user
            else :
                a.cliente = User.objects.get(pk=who)

            errore = "NO_ERRORS"
            for i in Appuntamento.objects.all() :
                if (a.data_ora_inizio >= i.data_ora_inizio and a.data_ora_inizio < i.data_ora_fine) or (
                        a.data_ora_fine > i.data_ora_inizio and a.data_ora_fine <= i.data_ora_fine) :
                    errore = "Appuntamento non disponibile, scelga un'altra data/ora"
                    c = Coda()
                    c.data_ora_inizio = a.data_ora_inizio
                    c.data_ora_fine = a.data_ora_fine
                    c.servizio = s
                    c.cliente = a.cliente
                    c.save()
        else :
            errore = "Non è possibile prenotare appuntamenti antecedenti ad ora!"
    else :
        errore = "Domenica e lunedì sono giorni di chiusura!"

    if errore != "NO_ERRORS" :
        date_alternative = nuove_date(s)

    else :
        try :
            a.save()
            print("Appuntamento salvato con successo! Servizio: " + a.servizio.nome + " prenotato per il: " + str(
                a.data_ora_inizio))
        except Exception as e :
            errore = "Errore nella registrazione dell'appuntamento"
            print(errore + " " + str(e))

    return render(request, "gestione/appuntamento.html", {"errore" : errore,
                                                          "servizio" : s,
                                                          "appuntamento" : a,
                                                          "alternativa" : date_alternative,
                                                          "who" : who}
                  )


@login_required()
def my_situation(request) :
    user = get_object_or_404(User, pk=request.user.pk)
    appuntamenti = user.appuntamenti_fissati.all()
    ctx = {"listappuntamenti" : appuntamenti}
    return render(request, "gestione/situazione.html", ctx)


class DeleteAppuntamentoView(LoginRequiredMixin, DeleteView) :
    model = Appuntamento
    template_name = "gestione/cancella_appuntamento.html"
    success_url = reverse_lazy('gestione:situazione')

    def get_context_data(self, **kwargs) :
        ctx = super().get_context_data(**kwargs)
        c = ctx["object"]

        for i in Coda.objects.all() :
            durata = (i.data_ora_fine - i.data_ora_inizio) / timedelta(minutes=1)
            if c.data_ora_inizio == i.data_ora_inizio and c.servizio.minuti <= durata :
                subject = "Appuntamento disponibile"
                body = "Gentile " + i.cliente.username + ", si sono liberati data e orario (" + str(
                    timezone.localtime(i.data_ora_inizio)) + ") che aveva " \
                                         "richiesto in precedenza per il Servizio " + i.servizio.nome + " della durata" \
                                                                                                        " di " + str(
                    i.servizio.minuti) + " minuti. Se è ancora di Suo interesse può prenotare" \
                                         " dal nostro sito www.ivonnestudio.com. La aspetto! Ivonne!"
                email = EmailMessage(subject, body, 'webmaster@localhost', [i.cliente.email])
                email.send()
                i.delete()
        return ctx


class StudioSituationView(GroupRequiredMixin, ListView) :
    titolo = "Stato Attuale degli Appuntamenti"
    group_required = ['Creativi']
    model = Appuntamento
    template_name = 'gestione/situazione_generale.html'

class AppuntamentoRicercaView(StudioSituationView) :
    titolo = "La ricerca ha prodotto i seguenti risultati"

    def get_queryset(self) :
        sstring = self.request.resolver_match.kwargs["sstring"]
        where = self.request.resolver_match.kwargs["where"]

        if "Data_ora_inizio" in where:
            utc = sstring.split("+")
            offset = utc[1].split(":")
            date_time = datetime.fromisoformat(sstring)
            date_time_utc = date_time - timedelta(hours=int(offset[0]))
            # print(sstring)
            sstring = date_time_utc.replace(tzinfo=None)
            # print(sstring)
            qq = self.model.objects.filter(data_ora_inizio__icontains=sstring)
        else:
            qq = self.model.objects.filter(cliente__username__icontains=sstring)
        return qq


class CreateServizioView(GroupRequiredMixin, CreateView) :
    group_required = ["Creativi"]
    model = Servizio
    title = "Inserimento Nuovo Servizio"
    form_class = ServizioForm
    template_name = "gestione/crea_servizio.html"
    success_url = reverse_lazy("gestione:listaservizi")


class DeleteServizioView(GroupRequiredMixin, DeleteView) :
    group_required = ["Creativi"]
    model = Servizio
    template_name = "gestione/cancella_servizio.html"
    success_url = reverse_lazy('gestione:listaservizi')

    def get_context_data(self, **kwargs) :
        ctx = super().get_context_data(**kwargs)
        c = ctx["object"]
        return ctx


class UpdateServizioView(GroupRequiredMixin, UpdateView):
    group_required = ["Creativi"]
    model = Servizio
    form_class = ServizioForm
    template_name = "gestione/modifica_servizio.html"
    success_url = reverse_lazy('gestione:listaservizi')

    def get_context_data(self, **kwargs) :
        ctx = super().get_context_data(**kwargs)
        c = ctx["object"]
        return ctx




