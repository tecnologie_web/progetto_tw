from django.test import TestCase
from datetime import datetime
from datetime import timedelta, time
from django.utils import timezone
from .views import nuove_date
from logging import getLogger
from .models import Appuntamento, Servizio
from django.contrib.auth.models import User
from django.urls import reverse


# Test della funzione nuove_date (in gestione/views.py)
class NuoveDateTests(TestCase):

    def setUp(self):
        self.u = User.objects.create_user(username="happyowl",
                                        email="gf.amministrazione@gmail.com",
                                        password="coocoo")
        self.s = Servizio.objects.create(nome="Color Wow!", minuti=120)
        return super().setUp()

    def date_time_create(self, date, hour):
        orario = time.fromisoformat(hour)
        date_time = datetime.combine(date, orario, tzinfo=timezone.get_current_timezone())
        return date_time

    def crea_appuntamento(self, date_time):
        a = Appuntamento.objects.create(servizio=self.s,
                                        cliente=self.u,
                                        data_ora_inizio=date_time,
                                        data_ora_fine=date_time + timedelta(minutes=self.s.minuti))
        a.save()
        return a

    # comportamento con db vuoto
    def test_nuove_date_con_db_vuoto(self):
        nd = nuove_date(self.s)
        a = timezone.now()
        b = a + timedelta(minutes=self.s.minuti)
        self.assertEqual(nd, [a,b], "Le date proposte non sono quelle attese")

    # appuntamenti nel passato
    def test_past_reservation(self):
        date_time = timezone.now() - timedelta(days=2)
        self.crea_appuntamento(date_time)
        nd = nuove_date(self.s)
        #print(nd)
        self.assertEqual(Appuntamento.objects.all().count(), 0, "Non devono esserci appuntamenti obsoleti")

    # slot temporale tra due appuntamenti non sufficiente (caso con data-ora finale in orario di chiusura)
    def test_nuove_date_senza_appuntamenti_successivi_ora_fine_non_ok(self):
        # definisco il "prossimo martedì" ricavando la data di oggi...
        today = datetime.today()
        # sottraendo i giorni già trascorsi questa settimana e aggiungendo una settimana ( = prossimo lunedì)...
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        # ...e aggiungendo un giorno. Si è così certi della creazione di un appuntamento in giorno di apertura
        next_tuesday = next_monday + timedelta(days=1)
        data_ora_inizio = self.date_time_create(next_tuesday, '17:30')
        data_ora_inizio2 = data_ora_inizio - timedelta(minutes=self.s.minuti)
        a = self.crea_appuntamento(data_ora_inizio2)
        a2 = self.crea_appuntamento(data_ora_inizio)
        expected_day = next_tuesday + timedelta(days=1)
        expected_datetime = self.date_time_create(expected_day, '08:00')
        expected_datetime2 = expected_datetime + timedelta(hours=7)
        nd = nuove_date(self.s)
        self.assertEqual(nd,[expected_datetime,expected_datetime2],"Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()

    # slot temporale tra due appuntamenti non sufficiente (caso con data-ora finale in orario di apertura)
    def test_nuove_date_senza_appuntamenti_successivi_ora_fine_ok(self):
        today = datetime.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        next_tuesday = next_monday + timedelta(days=1)
        data_ora_inizio = self.date_time_create(next_tuesday, '08:00')
        data_ora_inizio2 = data_ora_inizio + timedelta(minutes=self.s.minuti)
        a = self.crea_appuntamento(data_ora_inizio)
        a2 = self.crea_appuntamento(data_ora_inizio2)
        expected_datetime = a2.data_ora_fine
        expected_date2 = expected_datetime.date() + timedelta(days=1)
        expected_datetime2 = self.date_time_create(expected_date2,'08:00')
        nd = nuove_date(self.s)
        self.assertEqual(nd,[expected_datetime,expected_datetime2], "Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()



    # Controllo in caso di orario finale del primo appuntamento oltre l'orario di chiusura e di sabato
    # per controllo contestuale della funzione next_day_8 (vedi views.py)
    def test_nuove_date_slot_temporale_sab_non_ok_mart_ok(self):
        today = datetime.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        next_saturday = next_monday + timedelta(days=5)
        tuesday_after= next_saturday + timedelta(days=3)
        data_ora_inizio = self.date_time_create(next_saturday,'17:30')
        data_ora_inizio2 = self.date_time_create(tuesday_after,'14:00')
        a = self.crea_appuntamento(data_ora_inizio)
        a2 = self.crea_appuntamento(data_ora_inizio2)
        expected_datetime = self.date_time_create(tuesday_after,'08:00')
        expected_datetime2 = expected_datetime + timedelta(hours=8)
        nd = nuove_date(self.s)
        self.assertEqual(nd,[expected_datetime,expected_datetime2], "Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()

    def test_nuove_date_slot_temporale_mart_ok_sab_non_ok(self):
        today = datetime.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        next_tuesday = next_monday + timedelta(days=1)
        next_saturday = next_tuesday + timedelta(days=4)
        data_ora_inizio = self.date_time_create(next_tuesday,'14:00')
        data_ora_inizio2 = self.date_time_create(next_saturday,'17:30')
        a = self.crea_appuntamento(data_ora_inizio)
        a2 = self.crea_appuntamento(data_ora_inizio2)
        expected_datetime = data_ora_inizio + timedelta(minutes=self.s.minuti)
        expected_date2 = next_tuesday + timedelta(weeks=1)
        expected_datetime2 = self.date_time_create(expected_date2,'08:00')
        nd = nuove_date(self.s)
        self.assertEqual(nd, [expected_datetime,expected_datetime2], "Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()

    def test_nuove_date_slot_temporale_both_datetimes_not_ok(self) :
        today = datetime.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        next_saturday = next_monday + timedelta(days=5)
        saturday_after = next_saturday + timedelta(weeks=1)
        data_ora_inizio = self.date_time_create(next_saturday,'17:30')
        data_ora_inizio2 = self.date_time_create(saturday_after,'17:30')
        a = self.crea_appuntamento(data_ora_inizio)
        a2 = self.crea_appuntamento(data_ora_inizio2)
        expected_date = next_saturday + timedelta(days=3)
        expected_date2 = saturday_after + timedelta(days=3)
        expected_datetime = self.date_time_create(expected_date,'08:00')
        expected_datetime2 = self.date_time_create(expected_date2,'08:00')
        nd = nuove_date(self.s)
        self.assertEqual(nd, [expected_datetime,expected_datetime2], "Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()

    def test_nuove_date_slot_temporale_both_datetimes_ok(self):
        today = datetime.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        next_tuesday = next_monday + timedelta(days=1)
        data_ora_inizio = self.date_time_create(next_tuesday,'08:00')
        data_ora_inizio2 = self.date_time_create(next_tuesday,'14:00')
        a = self.crea_appuntamento(data_ora_inizio)
        a2 = self.crea_appuntamento(data_ora_inizio2)
        expected_datetime = a.data_ora_fine
        expected_datetime2 = a2.data_ora_fine
        nd = nuove_date(self.s)
        self.assertEqual(nd, [expected_datetime,expected_datetime2],"Le date proposte non sono quelle attese")
        a.delete()
        a2.delete()

    def tearDown(self):
        User.objects.get(username="happyowl").delete()
        Servizio.objects.get(nome="Color Wow!").delete()
        return super().tearDown()

# test della CBV ServizioRicercaView

# funzione per creare servizi
def create_service(nome, minuti, testo):
    return Servizio.objects.create(nome=nome, minuti=minuti, testo=testo)

class ServizioRicercaViewTests(TestCase):

    # test della ricerca di servizi con db vuoto
    def test_service_research_without_services(self):
        response = self.client.get(reverse('gestione:ricerca_risultati', kwargs={'sstring':'Piega', 'where':'Nome'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "La ricerca non ha prodotto alcun risultato")
        self.assertQuerysetEqual(response.context['object_list'],[])

    # creazione di due servizi in db e ricerca corretta con campo "Nome"
    def test_service_research_with_name_matches(self):
        a = create_service(nome='Piega Natural Dream', minuti=60, testo='Piega Natural Dream')
        b = create_service(nome='Victory Color', minuti=120, testo= 'Victory Color')
        response = self.client.get(reverse('gestione:ricerca_risultati', kwargs={'sstring' : 'Piega', 'where' : 'Nome'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tempo di esecuzione")
        self.assertQuerysetEqual(response.context['object_list'], [a])

    # creazione di due servizi in db e ricerca corretta con campo "Minuti"
    def test_service_research_with_minutes_matches(self) :
        a = create_service(nome='Piega Natural Dream', minuti=60, testo='Piega Natural Dream')
        b = create_service(nome='Victory Color', minuti=120, testo='Victory Color')
        response = self.client.get(reverse('gestione:ricerca_risultati', kwargs={'sstring' : '120', 'where' : 'Minuti'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tempo di esecuzione")
        self.assertQuerysetEqual(response.context['object_list'], [b])

    # creazione di due servizi in db e ricerca con campo errato
    def test_service_research_without_field_match(self):
        a = create_service(nome='Piega Natural Dream', minuti=60, testo='Piega Natural Dream')
        b = create_service(nome='Victory Color', minuti=120, testo='Victory Color')
        response = self.client.get(reverse('gestione:ricerca_risultati', kwargs={'sstring' : 'Piega', 'where' : 'Minuti'}))
        self.assertContains(response, "La ricerca non ha prodotto alcun risultato")
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [])

    # creazione di due servizi in db e ricerca con campo corretto, ma con esito negativo
    def test_service_research_with_correct_field_without_matches(self):
        a = create_service(nome='Piega Natural Dream', minuti=60, testo='Piega Natural Dream')
        b = create_service(nome='Victory Color', minuti=120, testo='Victory Color')
        response = self.client.get(reverse('gestione:ricerca_risultati', kwargs={'sstring' : 'Taglio', 'where' : 'Nome'}))
        self.assertContains(response, "La ricerca non ha prodotto alcun risultato")
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['object_list'], [])

