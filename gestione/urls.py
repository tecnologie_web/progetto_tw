from django.urls import path
from .views import *

app_name = "gestione"

urlpatterns = [
    path("", gestione_home, name="home"),
    path("listaservizi/", ServizioListView.as_view(),name="listaservizi"),
    path("ricerca/", search, name="cercaservizio"),
    path("ricerca/<str:sstring>/<str:where>/", ServizioRicercaView.as_view(), name="ricerca_risultati"),
    path("prenota/<pk>/", save_the_date, name="prenota"),
    path("appuntamento/<str:pk>/<str:when>/<str:who>", reservation, name="prenota_risultati"),
    path("situazione/", my_situation, name="situazione"),
    path("cancella_appuntamento/<pk>", DeleteAppuntamentoView.as_view(), name="cancella_appuntamento"),
    path("situazione_generale/", StudioSituationView.as_view(), name="situazione_gen"),
    path("crea_servizio/", CreateServizioView.as_view(), name="crea_servizio"),
    path("cancella_servizio/<pk>/", DeleteServizioView.as_view(), name="cancella_servizio"),
    path("modifica_servizio/<pk>", UpdateServizioView.as_view(), name="modifica_servizio"),
    path("ricerca_appuntamento/", search_reservation, name="ricerca_appuntamento"),
    path("ricerca_appuntamento/<str:sstring>/<str:where>/", AppuntamentoRicercaView.as_view(), name="appuntamento_risultati")
    ]
