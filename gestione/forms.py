import datetime as dt
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from .models import Servizio, User
from django.forms import ModelForm


class SearchForm(forms.Form):
    CHOICE_LIST = [("Nome", "Cerca tra i Servizi"), ("Minuti", "Cerca durata di esecuzione")]
    helper = FormHelper()
    helper.form_id = "search_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit","Cerca"))
    search_string = forms.CharField(label="Cerca", max_length=50,min_length=2,required=True)
    search_where = forms.ChoiceField(label="Dove", required=True, choices=CHOICE_LIST)

class AppuntamentoSearchForm(forms.Form):
    hours = [x for x in range(8, 18)]
    minutes = [0, 30]
    HOUR_CHOICES = [(dt.time(hour=x), '{:02d}'.format(x)) for x in hours]
    MINS_CHOICES = [(dt.time(minute=x), '{:02d}'.format(x)) for x in minutes]
    CHOICE_LIST = [("Data_ora_inizio", "Cerca data"), ("Cliente", "Cerca Cliente")]
    helper = FormHelper()
    helper.form_id = "appuntamento_search_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit", "Cerca"))
    search_string = forms.CharField(label="Username", max_length=50, min_length=2, required=False)
    giorno = forms.DateField(widget=forms.TextInput(attrs={'type' : 'date'}), label='Giorno', required=False)
    ora = forms.ChoiceField(label="Ora", required=False, choices=HOUR_CHOICES)
    mins = forms.ChoiceField(label="Minuti", required=False, choices=MINS_CHOICES)
    search_where = forms.ChoiceField(label="Dove", required=False, choices=CHOICE_LIST)

class BookForm(forms.Form):
    hours = [x for x in range(8,18)]
    minutes = [0,30]
    HOUR_CHOICES = [(dt.time(hour=x), '{:02d}'.format(x)) for x in hours]
    MINS_CHOICES = [(dt.time(minute=x), '{:02d}'.format(x)) for x in minutes]
    helper = FormHelper()
    helper.form_id = "book_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit","Ricerca"))
    giorno = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}), label='Giorno')
    # forzare l'orario di prenotazione ad un range fissato di orari accettati
    ora = forms.ChoiceField(label="Ora", required = True, choices=HOUR_CHOICES)
    mins = forms.ChoiceField(label="Minuti", required = True, choices=MINS_CHOICES)

class CreativeBookForm(BookForm):
    USER_CHOICES = []
    for u in User.objects.all():
        USER_CHOICES.append((u.pk,u.username))

    cliente = forms.ChoiceField(label="Cliente", required=True, choices=USER_CHOICES)

class ServizioForm(ModelForm):
    helper = FormHelper()
    helper.form_id = "service_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit", "Inserisci"))
    class Meta:
        model = Servizio
        fields = ['immagine', 'nome', 'minuti', 'testo']